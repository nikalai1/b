package com.example.fragments

import android.graphics.pdf.PdfDocument
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import com.example.fragments.adapters.ViewPagerFragmentAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {

    private lateinit var ViewPager: ViewPager2
    private lateinit var tabLayout: TabLayout
    private lateinit var ViewPager2: ViewPager2
    private lateinit var adapter: ViewPagerFragmentAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    init()

        ViewPager.adapter = adapter
        TabLayoutMediator(tabLayout,ViewPager) { tab,position ->
            tab.text = "tab " + (position + 1)

        }.attach()



    }

    fun init() {
        ViewPager = findViewById(R.id.ViewPager)
        ViewPager2 = findViewById(R.id.ViewPager2)
        tabLayout = findViewById(R.id.tabLayout)
        adapter = ViewPagerFragmentAdapter(this)
    }
}