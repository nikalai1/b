package com.example.fragments.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.fragments.fragments.ChiFragment
import com.example.fragments.fragments.TigFragment
import com.example.fragments.fragments.ZedFragment

class ViewPagerFragmentAdapter(activity: FragmentActivity): FragmentStateAdapter(activity) {
    override fun getItemCount() = 3

    override fun createFragment(position: Int): Fragment {
        return when(position) {
            0 -> ZedFragment()
            1 -> TigFragment()
            2 -> ChiFragment()
            else -> ZedFragment()
        }

    }
}